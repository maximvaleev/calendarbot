﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

public class Button
{
    public string Callback { get; set; }
    public string Name { get; set; }

    public Button(string callback, string name)
    {
        Callback = callback;
        Name = name;
    }
}
