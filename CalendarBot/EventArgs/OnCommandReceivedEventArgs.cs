﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot
{
    internal class OnCommandReceivedEventArgs : EventArgs
    {
        public string Command { get; }
        public UserData UserData { get; }

        public OnCommandReceivedEventArgs(string command, UserData userData)
        {
            Command = command;
            UserData = userData;
        }
    }
}
