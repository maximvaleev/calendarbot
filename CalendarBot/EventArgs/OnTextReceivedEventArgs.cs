﻿namespace CalendarBot
{
    internal class OnTextReceivedEventArgs : EventArgs
    {
        public long UserId { get; }
        public string MessageText { get; }

        public OnTextReceivedEventArgs(long userId, string messageText)
        {
            UserId = userId;
            MessageText = messageText;
        }
    }
}