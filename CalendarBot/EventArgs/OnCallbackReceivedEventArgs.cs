﻿namespace CalendarBot
{
    internal class OnCallbackReceivedEventArgs : EventArgs
    {
        public long UserId { get; }
        public string CallbackData { get; }

        public OnCallbackReceivedEventArgs(long userId, string callbackData)
        {
            UserId = userId;
            CallbackData = callbackData;
        }
    }
}