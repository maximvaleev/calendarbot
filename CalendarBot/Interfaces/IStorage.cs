﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal interface IStorage
{
    bool AddUser(UserStorageEntry userEntry);
    IEnumerable<UserStorageEntry> GetAllUsers();
    UserStorageEntry? GetUser(long userId);
    Dictionary<string, List<MyEvent>> GetAllEventsForDate(DateTime date);
    bool AddSubscription(long userId, string calName);
    bool RemoveSubscription(long userId, string calName);
}
