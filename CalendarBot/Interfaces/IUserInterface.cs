﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal interface IUserInterface
{
    event EventHandler<OnCommandReceivedEventArgs> OnCommandReceived;
    event EventHandler<OnTextReceivedEventArgs> OnTextReceived;
    event EventHandler<OnCallbackReceivedEventArgs> OnCallbackReceived;

    void CreateUiLanguageEntities(Language language);
    Task SendMessageAsync(UserData userData, string message, Menus menu = Menus.None);
    Task SendMessageAsync(UserData userData, string message, Menu menu);
    void ShutDown();
}
