﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal interface IUiState
{
    public void HandleUiCommand(string command);
    public void HandleUiTextMessage(string message);
    public void HandleUiCallback(string callbackData);
    public void EnterState(UiStates stateCameFrom);
}
