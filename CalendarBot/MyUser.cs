﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

/// <summary>
/// Рабочий класс пользователя в программе. Хранит данные о пользователе, контекст и бизнес-логику
/// </summary>
internal class MyUser
{
    public UserData UserData { get; }
    public UiContext UiContext { get; }
    // key - Subscription calendar name, value - today's events sent flag
    public Dictionary<string, bool> SubsPublished { get; set; }

    public MyUser(UserData userData, IUserInterface ui, Language language, Dictionary<string, List<MyEvent>> todayEvents, IStorage storage, IEnumerable<string>? subsNames)
    {
        UserData = userData;
        SubsPublished = new Dictionary<string,bool>();
        if (subsNames is not null) 
        { 
            foreach (string sub in subsNames) 
            { 
                SubsPublished.Add(sub, false);
            }
        }
        UiContext = new(userData, ui, language, SubsPublished, todayEvents, storage);
    }
}
