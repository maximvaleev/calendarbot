﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

/// <summary>
/// Сущность для отправки и извлечения данных о пользоветеле из Storage
/// </summary>
internal class UserStorageEntry
{
    // все нужные данные без деталей реализации бизнес-логики.
    public UserData UserData { get; private set; }
    public IEnumerable<string>? SubsNames { get; private set; }

    public UserStorageEntry(UserData userData, IEnumerable<string>? subsNames = null)
    {
        UserData = userData;
        if (subsNames is null)
        {
            SubsNames = new List<string>();
        }
        else
        {
            SubsNames = subsNames;
        }
    }
}
