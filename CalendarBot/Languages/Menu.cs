﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace CalendarBot;

/// <summary>
/// класс, описывающий одно меню UI на всех языках
/// </summary>
internal class Menu
{
    public List<Button[]> ButtonLayout { get; }
    public bool IsReplyKeyboard { get; }
    public Menus MenuType { get; }
    public Dictionary<string, Button> Buttons { get; }

    public Menu(Menus menuType, bool isReplyKeyboard)
    {
        MenuType = menuType;
        IsReplyKeyboard = isReplyKeyboard;
        Buttons = new Dictionary<string, Button>();
        ButtonLayout = new List<Button[]>(5);
    }

    public void AddButtonsRow(params Button[] buttons)
    {
        ButtonLayout.Add(buttons);
        foreach (Button button in buttons) 
        { 
            Buttons.Add(button.Callback, button);
        }
    }

    public HashSet<string> GetCallbacks()
    {
        return new HashSet<string>(Buttons.Keys);
    }

    public HashSet<string> GetButtonNames()
    {
        return new HashSet<string>(Buttons.Values.Select(x => x.Name));
    }
}
