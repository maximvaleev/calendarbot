﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

/// <summary>
/// Класс, содержащий в себе все меню и сообщения в UI на всех языках. Весь текст, который окажется в UI должен прописываться здесь.
/// </summary>
internal class Language
{
    public Dictionary<Menus, Menu> AllMenus { get; }
    public Dictionary<Messages, string> AllMessages { get; }

    public Language()
    {
        AllMenus = new Dictionary<Menus, Menu>();
        AllMessages = new Dictionary<Messages, string>();

        BuildMenus();
        BuildMessages();
    }

    private void BuildMenus()
    {
        // *** Main menu *** (главное меню - ReplyKeyboard)
        Menu mainMenu = new(Menus.Main, true);
        AllMenus.Add(Menus.Main, mainMenu);
        //Объявляем все кнопки
        Button subsButton = new("MySubscriptions", "Мои подписки");
        Button prefsButton = new("Preferences", "Настройки");
        Button futureEventsButton = new("FutureEvents", "Предстоящие события");
        Button customCalsButton = new("CustomCalendars", "Пользовательские календари");
        // Добавляем кнопки в меню построчно
        mainMenu.AddButtonsRow(subsButton, prefsButton);
        mainMenu.AddButtonsRow(futureEventsButton, customCalsButton);

        // *** Calendar selection menu *** (меню выбора календарей для подписки - InlineKeyboard)
        Menu calSelMenu = new(Menus.CalendarSelection, false);
        AllMenus.Add(Menus.CalendarSelection, calSelMenu);
        //Объявляем все кнопки
        Button calendar1 = new("InternationalEvents", "Международные праздники");
        Button calendar2 = new("ITDay", "День в IT");
        Button goBack = new("Back", "⬅️ Назад");
        // Добавляем кнопки в меню построчно
        calSelMenu.AddButtonsRow(calendar1);
        calSelMenu.AddButtonsRow(calendar2);
        calSelMenu.AddButtonsRow(goBack);
    }

    private void BuildMessages()
    {
        string message;

        // Hello - приветственное сообщение при получении команды /start
        message = "Добро пожаловать в бот-календарь! Для начала, подпишитесь на интересующие Вас рассылки.";
        AllMessages.Add(Messages.Hello, message);

        // Help - сообщение с подсказками при получении команды /help
        message = "Помоги себе сам и друзья тебя больше полюбят.\n\t\t    ~ Джонатан Свифт";
        AllMessages.Add(Messages.Help, message);

        // CantHandleRequest - когда что то пошло не так
        message = "Не удалось обработать запрос";
        AllMessages.Add(Messages.CantHandleRequest, message);
    }
}
