﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

/// <summary>
/// Меню для вывода в UI
/// </summary>
public enum Menus
{
    None,
    Remove,
    Custom,
    Main,
    CalendarSelection
}
