﻿namespace CalendarBot;

internal class Program
{
    static void Main()
    {
        Console.WriteLine("Hello world!");
        // Создание и инициализация всех слоев
        IUserInterface userInterface = CreateUserInterface();
        IStorage storage = CreateStorage();
        BusinessLayer businessLayer = new(userInterface, storage);

        Console.ReadKey();
        userInterface.ShutDown();
    }

    private static IStorage CreateStorage()
    {
        return new DatabaseLayer(@"User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=Calendar_bot;");
    }

    private static IUserInterface CreateUserInterface()
    {
        return new TelegramFacade("6498303434:AAGJwdzHXY08nz1orUhXbNI0KR0nhNOwIzs");
    }
}