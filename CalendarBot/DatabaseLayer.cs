﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace CalendarBot;

internal class DatabaseLayer : IStorage
{
    private readonly string _connString;

    public DatabaseLayer(string connString) => _connString = connString;

    private class UserDB
    {
        public long User_id { get; private set; }
        public long Chat_id { get; private set; }
        public string? User_name { get; private set; }
        public string First_name { get; private set; }
        public string? Last_name { get; private set; }

        public UserDB(long user_id, long chat_id, string? user_name, string first_name, string? last_name)
        {
            User_id = user_id;
            Chat_id = chat_id;
            User_name = user_name;
            First_name = first_name;
            Last_name = last_name;
        }
    }

    private class EventDB
    {
        public string CalName { get; private set; }
        public string Name { get; private set; }
        public DateTime Date { get; private set; }
        public string BriefContent { get; private set; }
        public string DetailedContent { get; private set; }

        public EventDB(string calname, string name, DateTime date, string briefcontent, string detailedcontent)
        {
            CalName = calname;
            Name = name;
            Date = date;
            BriefContent = briefcontent;
            DetailedContent = detailedcontent;
        }
    }

    public bool AddUser(UserStorageEntry userEntry)
    {
        try
        {
            using NpgsqlConnection conn = new(_connString);
            // вставить основные данные пользователя
            string sql = @"INSERT INTO public.user(user_id, chat_id, user_name, first_name, last_name) 
                     VALUES (@UserId, @ChatId, @UserName, @FirstName, @LastName)";
            UserData ud = userEntry.UserData;
            int affectedRows = conn.Execute(sql,
                new { ud.UserId, ud.ChatId, ud.UserName, ud.FirstName, ud.LastName });
            if (affectedRows == 0)
            {
                return false;
            }
            if (userEntry.SubsNames is null) 
            { 
                return true;
            }
            // Вставить данные о подписках пользователя
            foreach (string subName in userEntry.SubsNames)
            {
                // найти ID календаря по его имени
                sql = @"SELECT calendar_id FROM calendar WHERE name = @subName";
                int calId = conn.QueryFirstOrDefault<int>(sql, new { subName });
                // вставить в таблицу подписок запись о подписке пользователя на календарь
                sql = @"INSERT INTO subscription(user_id, calendar_id) 
                     VALUES (@UserId, @calId)";
                _ = conn.Execute(sql, new { ud.UserId, calId });
            }
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось добавить инфо о пользователе в БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return false;
        }
    }

    public Dictionary<string, List<MyEvent>> GetAllEventsForDate(DateTime date)
    {
        Dictionary<string, List<MyEvent>> resultDict = new();
        try
        {
            using NpgsqlConnection conn = new(_connString);
            date = new DateTime(date.Year, date.Month, date.Day);
            // получить данные всех пользователей
            string sql = @"SELECT cal.name as calname, evt.name as name, evt.event_date as date, evt.brief_content as briefcontent, evt.detailed_content as detailedcontent 
                FROM public.event as evt 
                JOIN public.calendar_event as cal_evt ON evt.event_id = cal_evt.event_id
                JOIN public.calendar as cal ON cal_evt.calendar_id = cal.calendar_id 
                WHERE evt.event_date = @date";
            IEnumerable<EventDB> eventsFromDb = conn.Query<EventDB>(sql, new { date });
            foreach (EventDB evt in eventsFromDb)
            {
                if (!resultDict.ContainsKey(evt.CalName))
                {
                    resultDict.Add(evt.CalName, new List<MyEvent>());
                }
                MyEvent myEvent = new(evt.Name, evt.CalName, evt.Name, evt.Date, evt.BriefContent, evt.DetailedContent);
                resultDict[evt.CalName].Add(myEvent);
            }
            return resultDict;
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось извлечь события из БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return resultDict;
        }
    }

    public IEnumerable<UserStorageEntry> GetAllUsers()
    {
        try
        {
            using NpgsqlConnection conn = new(_connString);
            // получить данные всех пользователей
            string sql = "SELECT * FROM public.user";
            IEnumerable<UserDB> usersFromDb = conn.Query<UserDB>(sql);
            List<UserStorageEntry> resultList = new();
            foreach (UserDB userDB in usersFromDb)
            {
                // Получить список имен календарей на которые подписан пользователь
                sql = @"SELECT cal.name  FROM public.subscription as sub 
                    JOIN public.user as usr ON usr.user_id = sub.user_id 
                    JOIN public.calendar as cal ON sub.calendar_id = cal.calendar_id 
                    WHERE usr.user_id = @userId";
                IEnumerable<string> subscriptions = conn.Query<string>(sql, new { userId = userDB.User_id });
                UserData userData = new(userDB.User_id, userDB.Chat_id, userDB.First_name, userDB.Last_name, userDB.User_name);
                resultList.Add(new UserStorageEntry(userData, subscriptions));
            }
            return resultList;
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось получить данные пользователей из БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return null;
        }
    }

    public UserStorageEntry? GetUser(long userId)
    {
        try
        {
            using NpgsqlConnection conn = new(_connString);
            // получить данные пользователя с указанным user_id
            string sql = "SELECT * FROM public.user WHERE user_id = @userId";
            UserDB? userDB = conn.QueryFirstOrDefault<UserDB>(sql, new { userId });
            if (userDB is null)
            {
                return null;
            }
            // Получить список имен календарей на которые подписан пользователь
            sql = "SELECT cal.name  FROM public.subscription as sub " +
                "JOIN public.user as usr ON usr.user_id = sub.user_id " +
                "JOIN public.calendar as cal ON sub.calendar_id = cal.calendar_id " +
                "WHERE usr.user_id = @userId";
            IEnumerable<string> subscriptions = conn.Query<string>(sql, new { userId });

            UserData userData = new(userDB.User_id, userDB.Chat_id, userDB.First_name, userDB.Last_name, userDB.User_name);
            return new UserStorageEntry(userData, subscriptions);
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось получить данные пользователя из БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return null;
        }
    }

    public bool AddSubscription(long userId, string calName)
    {
        try
        {
            using NpgsqlConnection conn = new(_connString);
            // найти ID календаря по его имени
            string sql = @"SELECT calendar_id FROM calendar WHERE name = @calName";
            int calId = conn.QueryFirstOrDefault<int>(sql, new { calName });
            if (calId == 0)
            {
                throw new Exception("Не удалось получить ID календаря из БД.");
            }
            // добавить подписку
            sql = @"INSERT INTO subscription(user_id, calendar_id) 
                     VALUES (@userId, @calId)";
            int affectedRows = conn.Execute(sql, new { userId, calId });
            if (affectedRows == 0)
            {
                return false;
            }
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось добавить подписку в БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return false;
        }
    }

    public bool RemoveSubscription(long userId, string calName)
    {
        try
        {
            using NpgsqlConnection conn = new(_connString);
            // найти ID календаря по его имени
            string sql = @"SELECT calendar_id FROM calendar WHERE name = @calName";
            int calId = conn.QueryFirstOrDefault<int>(sql, new { calName });
            if (calId == 0)
            {
                throw new Exception("Не удалось получить ID календаря из БД.");
            }
            // удалить подписку
            sql = @"DELETE FROM subscription WHERE user_id = @userId AND calendar_id = @calId";
            int affectedRows = conn.Execute(sql, new { userId, calId });
            if (affectedRows == 0)
            {
                return false;
            }
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine("Не удалось удалить подписку из БД: " + exc.Message);
#if DEBUG
            throw;
#endif
            return false;
        }
    }
}
