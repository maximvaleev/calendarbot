﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal class MyEvent
{
    public string Name { get; }
    public string CalendarName { get; }
    public string Callback { get; }
    public DateTime Date { get; }
    public string BriefContent { get; }
    public string DetailedContent { get; }

    public MyEvent(string name, string calendarName, string callback, DateTime date, string briefContent, string detailedContent)
    {
        Name = name;
        CalendarName = calendarName;
        Callback = callback;
        Date = date;
        BriefContent = briefContent;
        DetailedContent = detailedContent;
    }
}
