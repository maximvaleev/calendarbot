﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace CalendarBot;

internal class TelegramFacade : IUserInterface
{
    private readonly TelegramBotClient _bot;
    private readonly CancellationTokenSource _cts;
    public event EventHandler<OnCommandReceivedEventArgs>? OnCommandReceived;
    public event EventHandler<OnTextReceivedEventArgs>? OnTextReceived;
    public event EventHandler<OnCallbackReceivedEventArgs>? OnCallbackReceived;
    // переделать private readonly ReplyKeyboardMarkup mainReplyKeyboard;
    private readonly Dictionary<Menus, IReplyMarkup> _menus;

    public TelegramFacade(string token)
	{
		_bot = new TelegramBotClient(token);
        _cts = new CancellationTokenSource();
        _menus = new Dictionary<Menus, IReplyMarkup>();

        ReceiverOptions receiverOptions = new()
        {
            AllowedUpdates = new UpdateType[] { UpdateType.Message, UpdateType.CallbackQuery, UpdateType.Unknown}
            // изначальная опция, принимавшая все апдейты.
            //AllowedUpdates = Array.Empty<UpdateType>() // receive all update types except ChatMember related updates
        };

        _bot.StartReceiving(
            updateHandler: HandleUpdateAsync,
            pollingErrorHandler: HandlePollingErrorAsync,
            receiverOptions: receiverOptions,
            cancellationToken: _cts.Token
        );
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(update));

        switch (update.Type)
        {
            case UpdateType.Message:
                await HandleMessageTypedUpdateAsync(botClient, update, cancellationToken);
                break;
            case UpdateType.CallbackQuery:
                HandleCallbackTypedUpdate(update);
                break;
            case UpdateType.Unknown:
                if (update.Message is null)
                {
                    return;
                }
                _ = await botClient.SendTextMessageAsync(chatId: update.Message.Chat,
                    text: "Запрос не распознан.",
                    cancellationToken: cancellationToken);
                break;
            default:
                throw new NotImplementedException("Нет обработчика для UpdateType, указанного в AllowedUpdates");
        }
    }

    private async Task HandleMessageTypedUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Message is null)
        {
            return;
        }
        string? messageText = update.Message.Text;
        if (messageText is not null && update.Message.From is not null && !update.Message.From.IsBot)
        {
            // Если пришла команда
            if (messageText.StartsWith('/'))
            {
                _ = await botClient.SendTextMessageAsync(
                    chatId: update.Message.Chat, 
                    text: $"Получена команда \"{messageText}\"!",
                    cancellationToken: cancellationToken);
                // вызвать событие о команде. Передать в него данные о пользователе и команду без слэша.
                string command = messageText[1..].Trim().ToLower();
                OnCommandReceived?.Invoke(this, new OnCommandReceivedEventArgs(command, GetUserData(update)));
                return;
            }
            // если пришло текстовое сообщение от пользователя (или пользователь нажал на кнопку Reply Keyboard)
            OnTextReceived?.Invoke(this, new OnTextReceivedEventArgs(
                    update.Message.From.Id,
                    messageText));
        }
    }

    private void HandleCallbackTypedUpdate(Update update)
    {
        if (update.CallbackQuery is null)
        {
            throw new Exception("Странные вещи происходят. Проверь как работает CallbackQuery.");
        }
        if (update.CallbackQuery.Data is null)
        {
            throw new Exception("Callback data не должна быть null. Проверь не забыл ли ты отправить ее вместе с клавиатурой.");
        }
        OnCallbackReceived?.Invoke(this, new OnCallbackReceivedEventArgs(
                    update.CallbackQuery.From.Id,
                    update.CallbackQuery.Data));
    }

        private static Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
    {
        var ErrorMessage = exception switch
        {
            ApiRequestException apiRequestException
                => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
            _ => exception.ToString()
        };

        Console.WriteLine(ErrorMessage);
        return Task.CompletedTask;
    }

    public void ShutDown()
    {
        // Send cancellation request to stop bot
        _cts.Cancel();
        _cts.Dispose();
    }

    private static UserData GetUserData(Update update)
    {
        if (update.Type == UpdateType.Message)
        {
            if (update.Message is null || update.Message.From is null)
            {
                throw new Exception("ошибка в коде, найди другой подход к извлечению данных.");
            }
            return new UserData(
                userId: update.Message.From.Id,
                chatId: update.Message.Chat.Id,
                firstName: update.Message.From.FirstName,
                lastName: update.Message.From.LastName,
                userName: update.Message.From.Username
                );
        }
        else
        {
            // Если понадобится извлекать данные из других типов апдейтов, то метод нужно будет доработать
            throw new NotImplementedException();
        }
    }

    public async Task SendMessageAsync(UserData userData, string message, Menus menuType = Menus.None)
    {
        IReplyMarkup? replyMarkup = menuType switch
        {
            Menus.None => null,
            Menus.Remove => new ReplyKeyboardRemove(),// возможно в будущем заготовить эти клавиатуры заранее, а не создавать каждый раз
            Menus.Main => _menus[Menus.Main],
            Menus.CalendarSelection => _menus[Menus.CalendarSelection],
            _ => throw new NotImplementedException(),
        };

        _ = await _bot.SendTextMessageAsync(
                    chatId: userData.ChatId,
                    text: message,
                    replyMarkup: replyMarkup,
                    cancellationToken: _cts.Token);
    }

    // Конвертер менюшек класса Language в телеграммовские менюшки каждого из языков.
    public void CreateUiLanguageEntities(Language language)
    {
        // Меню
        foreach (KeyValuePair<Menus, Menu> menuKVP in language.AllMenus)
        {
            Menus menuType = menuKVP.Key;
            Menu menu = menuKVP.Value;
            // перебрать все языки
            // добавить в коллекцию менюшек.
            if (menu.IsReplyKeyboard)
            {
                _menus.Add(menuType, ConvertMenuToReplyKeyboard(menu));
            }
            else
            {
                _menus.Add(menuType, ConvertMenuToInlineKeyboard(menu));
            }
        }
    }

    private static InlineKeyboardMarkup ConvertMenuToInlineKeyboard(Menu menu)
    {
        InlineKeyboardButton[][] keyboardButtons = new InlineKeyboardButton[menu.ButtonLayout.Count][];
        // по каждому ряду
        for (int i = 0; i < keyboardButtons.Length; i++)
        {
            keyboardButtons[i] = new InlineKeyboardButton[menu.ButtonLayout[i].Length];
            // по каждой кнопке в ряду
            for (int j = 0; j < keyboardButtons[i].Length; j++)
            {
                keyboardButtons[i][j] = InlineKeyboardButton.WithCallbackData(
                    menu.ButtonLayout[i][j].Name,
                    menu.ButtonLayout[i][j].Callback);
            }
        }
        return new InlineKeyboardMarkup(keyboardButtons);
    }

    private static ReplyKeyboardMarkup ConvertMenuToReplyKeyboard(Menu menu)
    {
        KeyboardButton[][] keyboardButtons = new KeyboardButton[menu.ButtonLayout.Count][];
        // по каждому ряду
        for (int i = 0; i < keyboardButtons.Length; i++)
        {
            keyboardButtons[i] = new KeyboardButton[menu.ButtonLayout[i].Length];
            // по каждой кнопке в ряду
            for (int j = 0; j < keyboardButtons[i].Length; j++)
            {
                keyboardButtons[i][j] = new KeyboardButton(menu.ButtonLayout[i][j].Name);
            }
        }
        return new ReplyKeyboardMarkup(keyboardButtons);
    }

    public async Task SendMessageAsync(UserData userData, string message, Menu menu)
    {
        IReplyMarkup? replyMarkup;
        if (menu.IsReplyKeyboard)
        {
            replyMarkup = ConvertMenuToReplyKeyboard(menu);
        }
        else
        {
            replyMarkup = ConvertMenuToInlineKeyboard(menu);
        }

        _ = await _bot.SendTextMessageAsync(
                    chatId: userData.ChatId,
                    text: message,
                    replyMarkup: replyMarkup,
                    cancellationToken: _cts.Token);
    }
}
