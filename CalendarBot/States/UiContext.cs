﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal class UiContext : IUiState
{
    public IUiState CurrentState { get; private set; }
    public UiStates CurrentStateEnum { get; private set; }
    public UserData UserData { get; }
    public IUserInterface UI { get; }
    public Language Language { get; }
    public IStorage Storage { get; }
    public Dictionary<string, bool> SubsPublished { get; }
    public Dictionary<string, List<MyEvent>> TodayEvents { get; }
    private readonly Dictionary<UiStates, IUiState> _states;

    public UiContext(UserData userData, IUserInterface ui, Language language, Dictionary<string, bool> subsPublished, Dictionary<string, List<MyEvent>> todayEvents, IStorage storage)
    {
        UserData = userData;
        UI = ui;
        Language = language;
        SubsPublished = subsPublished;
        TodayEvents = todayEvents;
        Storage = storage;

        _states = new()
        {
            { UiStates.Main, new MainState(this) },
            { UiStates.CalendarSelection, new CalendarSelectionState(this) }
        };
        // чтобы подавить предупреждение что CurrentState не определен при выходе из конструктора
        CurrentState = _states[UiStates.Main];
        CurrentStateEnum = UiStates.Main;
        ChangeState(UiStates.Main);
    }

    public void HandleUiCallback(string callbackData)
    {
        CurrentState.HandleUiCallback(callbackData);
    }

    public void HandleUiCommand(string command)
    {
        CurrentState.HandleUiCommand(command);
    }

    public void HandleUiTextMessage(string message)
    {
        CurrentState.HandleUiTextMessage(message);
    }

    public void ChangeState(UiStates state) 
    {
        UiStates lastState = CurrentStateEnum;
        CurrentState = _states[state];
        CurrentStateEnum = state;
        CurrentState.EnterState(lastState);
    }

    public void EnterState(UiStates stateCameFrom)
    {
        throw new Exception("Нельзя войти в состояние UiContext.");
    }
}
