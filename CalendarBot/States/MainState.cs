﻿namespace CalendarBot;

internal class MainState : IUiState
{
    private readonly UiContext _context;
    private readonly Language _lang;
    private readonly IUserInterface _ui;
    private readonly UserData _userData;

    public MainState(UiContext context)
    {
        _context = context;
        _lang = _context.Language;
        _ui = _context.UI;
        _userData = _context.UserData;
    }

    public void EnterState(UiStates stateCameFrom)
    {
        _ui.SendMessageAsync(_userData, "Главное меню", Menus.Main);
    }

    public void HandleUiCallback(string callbackData)
    {
        foreach (var sub in _context.SubsPublished.Keys)
        {
            if (_context.TodayEvents.ContainsKey(sub))
            {
                MyEvent? evt = _context.TodayEvents[sub].Find(x => x.Callback == callbackData);
                if (evt is not null)
                {
                    _ui.SendMessageAsync(_userData, evt.DetailedContent);
                    return;
                }
            }
        }
        _ui.SendMessageAsync(_userData, _lang.AllMessages[Messages.CantHandleRequest]);
    }

    public void HandleUiCommand(string command)
    {
        bool commandFound = Enum.TryParse(command, out Commands commandEnum);
        if (commandFound) 
        {
            string message;
            switch (commandEnum) 
            {
                case Commands.start:
                    message = _lang.AllMessages[Messages.Hello];
                    _ui.SendMessageAsync(_userData, message, Menus.Main);
                    break;
                case Commands.help:
                    message = _lang.AllMessages[Messages.Help];
                    _ui.SendMessageAsync(_userData, message, Menus.None);
                    break;
                default:
                    throw new NotImplementedException($"Не реализован обработчик команды {command}");
            }
        }
        else
        {
            _ui.SendMessageAsync(_userData, _lang.AllMessages[Messages.CantHandleRequest], Menus.None);
        }
    }

    public void HandleUiTextMessage(string message)
    {
        HashSet<string> buttonNames = _lang.AllMenus[Menus.Main].GetButtonNames();
        if (buttonNames.Contains(message))
        {
            switch (message)
            {
                case "Мои подписки": // переходим в меню подписок, убираем главную клавиатуру
                    _context.ChangeState(UiStates.CalendarSelection);
                    break;
            }
        }
    }

}