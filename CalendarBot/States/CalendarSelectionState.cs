﻿using Telegram.Bot.Types;

namespace CalendarBot;

internal class CalendarSelectionState : IUiState
{
    private readonly UiContext _context;
    private readonly Language _lang;
    private readonly IUserInterface _ui;
    private readonly UserData _userData;
    private readonly Dictionary<string, bool> _subs;

    public CalendarSelectionState(UiContext context)
    {
        _context = context;
        _lang = _context.Language;
        _ui = _context.UI;
        _userData = _context.UserData;
        _subs = _context.SubsPublished;
    }

    public void EnterState(UiStates stateCameFrom)
    {
        if (stateCameFrom != UiStates.CalendarSelection)
        {
            _ui.SendMessageAsync(_userData, "Управление подписками", Menus.Remove);
        }
        
        string mes;
        if (_subs.Count > 0)
        {
            mes = "Активные подписки:\n\r";
            foreach (string sub in _subs.Keys)
            {
                mes += $"🔸 {sub}\n\r";
            }
        }
        else
        {
            mes = "У Вас нет активных подписок.\n\r";
        }
        mes += "\n\rдля подписки или отписки выберите пункт:";
        _ui.SendMessageAsync(_userData, mes, Menus.CalendarSelection);
    }

    public void HandleUiCallback(string callbackData)
    {
        HashSet<string> buttonCallbacks = _lang.AllMenus[Menus.CalendarSelection].GetCallbacks();
        if (buttonCallbacks.Contains(callbackData))
        {
            switch (callbackData)
            {
                case "InternationalEvents":
                    if (_subs.ContainsKey("Международные праздники"))
                    {
                        _subs.Remove("Международные праздники");
                        _context.Storage.RemoveSubscription(_userData.UserId, "Международные праздники");
                    }
                    else
                    {
                        _subs.Add("Международные праздники", false);
                        // TODO: сделать что-то если подписка не записалась в БД
                        _context.Storage.AddSubscription(_userData.UserId, "Международные праздники");
                    }
                    break;
                case "ITDay":
                    if (_subs.ContainsKey("Дата в IT"))
                    {
                        _subs.Remove("Дата в IT");
                        _context.Storage.RemoveSubscription(_userData.UserId, "Дата в IT");
                    }
                    else
                    {
                        _subs.Add("Дата в IT", false);
                        // TODO: сделать что-то если подписка не записалась в БД
                        _context.Storage.AddSubscription(_userData.UserId, "Дата в IT");
                    }
                    break;
                case "Back":
                    _context.ChangeState(UiStates.Main);
                    return;
                default:
                    throw new NotImplementedException();
            }
            EnterState(UiStates.CalendarSelection);
        }
        else
        {
            throw new NotImplementedException();
        }
    }

    public void HandleUiCommand(string command)
    {
        bool commandFound = Enum.TryParse(command, out Commands commandEnum);
        if (commandFound)
        {
            string message;
            switch (commandEnum)
            {
                case Commands.start:
                    message = _lang.AllMessages[Messages.Hello];
                    _ui.SendMessageAsync(_userData, message, Menus.Main);
                    break;
                case Commands.help:
                    message = _lang.AllMessages[Messages.Help];
                    _ui.SendMessageAsync(_userData, message, Menus.None);
                    break;
                default:
                    throw new NotImplementedException($"Не реализован обработчик команды {command}");
            }
        }
        else
        {
            _ui.SendMessageAsync(_userData, _lang.AllMessages[Messages.CantHandleRequest], Menus.None);
        }
        _context.ChangeState(UiStates.Main);
    }

    public void HandleUiTextMessage(string message)
    {
        _ui.SendMessageAsync(_userData, _lang.AllMessages[Messages.CantHandleRequest]);
        _context.ChangeState(UiStates.Main);
    }
}