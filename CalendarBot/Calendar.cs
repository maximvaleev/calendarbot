﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarBot;

internal class Calendar
{
    public string Name { get; }
    public Dictionary<DateTime, List<MyEvent>> Events {  get; }

    public Calendar(string name)
    {
        Name = name;
        Events = new Dictionary<DateTime, List<MyEvent>>();
    }

    public void AddEvent(DateTime date, MyEvent myEvent)
    {
        if (!Events.ContainsKey(date))
        {
            Events.Add(date, new List<MyEvent>());
        }
        Events[date].Add(myEvent);
    }
}
