﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace CalendarBot;

internal class BusinessLayer
{
    private readonly IUserInterface _ui;
    private readonly IStorage _storage;
    // Ключ - UserId
    private readonly Dictionary<long, MyUser> _users;
    private readonly Language _language;
    // key - Calendar name, value - list of events
    private readonly Dictionary<string, List<MyEvent>> _todayEvents;
    private DateTime _lastPublishingDate;

    public BusinessLayer(IUserInterface userInterface, IStorage storage)
    {
        _storage = storage;
        _language = new Language();
        _todayEvents = new();

        _ui = userInterface;
        _ui.CreateUiLanguageEntities(_language);
        _ui.OnCommandReceived += OnCommandReceivedEventHandler;
        _ui.OnTextReceived += OnTextReceivedEventHandler;
        _ui.OnCallbackReceived += OnCallbackReceivedEventHandler;

        _users = new();
        PopulateUsersDict();

        _lastPublishingDate = DateTime.Now.AddDays(-1.0);
        _ = PublishCalendarsAsync(new CancellationToken());
    }

    private void PopulateUsersDict()
    {
        IEnumerable<UserStorageEntry> userEntities = _storage.GetAllUsers();
        foreach (UserStorageEntry userEntity in userEntities)
        {
            AddUserToDict(userEntity);
        }
    }

    private void OnCallbackReceivedEventHandler(object? sender, OnCallbackReceivedEventArgs e)
    {
        Console.WriteLine($"Callback received by business layer:");
        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));

        MyUser currentUser;
        try
        {
            currentUser = GetCurrentUser(e.UserId);
        }
        catch (Exception exc)
        {
            Console.WriteLine("Ошибка при получении текущего пользователя: " + exc.Message);
#if DEBUG
            throw;
#endif
            return;
        }
        currentUser.UiContext.HandleUiCallback(e.CallbackData);
    }

    private MyUser GetCurrentUser(long userId, UserData? userData = null)
    {
        if (!_users.ContainsKey(userId))
        {
            // может он есть в Storage?
            UserStorageEntry? entry = _storage.GetUser(userId);
            if (entry is null)
            {
                // создать юзера и сохранить в Storage
                if (userData is null)
                {
                    throw new Exception("Нет данных для добавления пользователя в БД");
                }
                entry = new(userData);
                // TODO: сделать что-то если юзер не добавился в БД
                _ = _storage.AddUser(entry);
            }
            AddUserToDict(entry);
        }
        return _users[userId];
    }

    private void OnTextReceivedEventHandler(object? sender, OnTextReceivedEventArgs e)
    {
        Console.WriteLine($"Text received by business layer:");
        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));

        MyUser currentUser;
        try
        {
            currentUser = GetCurrentUser(e.UserId);
        }
        catch (Exception exc)
        {
            Console.WriteLine("Ошибка при получении текущего пользователя: " + exc.Message);
#if DEBUG
            throw;
#endif
            return;
        }
        currentUser.UiContext.HandleUiTextMessage(e.MessageText);
    }

    private void OnCommandReceivedEventHandler (object? sender, OnCommandReceivedEventArgs e)
    {
        Console.WriteLine($"Command received by business layer:");
        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(e));

        MyUser currentUser;
        try
        {
            currentUser = GetCurrentUser(e.UserData.UserId, e.UserData);
        }
        catch (Exception exc)
        {
            Console.WriteLine("Ошибка при получении текущего пользователя: " + exc.Message);
#if DEBUG
            throw;
#endif
            return;
        }
        currentUser.UiContext.HandleUiCommand(e.Command);
    }

    private void AddUserToDict(UserStorageEntry userEntry)
    {
        MyUser newUser = new(userEntry.UserData, _ui, _language, _todayEvents, _storage, userEntry.SubsNames);
        _users.Add(newUser.UserData.UserId, newUser);
    }

    private async Task PublishCalendarsAsync(CancellationToken cancellationToken)
    {
        StringBuilder sb = new();
        while (!cancellationToken.IsCancellationRequested)
        {
            DateTime now = DateTime.Now;

            // если сейчас неудобное для рассылки время, то просто ждем
            //if (now.Hour is < 10 or > 21)
            //{
            //    await Task.Delay(30000, CancellationToken.None); // 300 000 = 5 минут
            //    continue;
            //}

            // Если мы перешли в завтра
            if (_lastPublishingDate.Day != now.Day)
            {
                // получить новую рассылку из Storage
                var result_todayEvents = _storage.GetAllEventsForDate(now);
                foreach (var item in result_todayEvents)
                {
                    _todayEvents.Add(item.Key, item.Value);
                }
                // убрать все отметки у пользователей о том, что им что то было уже отправлено.
                foreach(MyUser user in _users.Values)
                {
                    foreach (string sub in user.SubsPublished.Keys)
                    {
                        user.SubsPublished[sub] = false;
                    }
                }
            }

            // Найдем хотябы одного пользователя, которому нужна хотябы одна из наших подписок
            string? subToSend = null;
            foreach (MyUser user in _users.Values)
            {
                foreach (KeyValuePair<string, bool> subKVP in user.SubsPublished)
                {
                    if (subKVP.Value is false)
                    {
                        if (_todayEvents.ContainsKey(subKVP.Key))
                        {
                            subToSend = subKVP.Key;
                            break;
                        }
                    }
                }
            }
            // если мы все на сегодня отправили, то ждем
            if (subToSend is null) 
            {
                await Task.Delay(30000, CancellationToken.None); // 300 000 = 5 минут
                continue;
            }
            // у нас есть запрос на рассылку хотябы одного календаря. Его и отправим в этот проход. Остальное в следующие проходы.

            // Сформировать сообщение, которое будем отправлять пользователям
            sb.Clear();
            sb.Append($"🗓 *** {now.ToString("dd MMMM yyyy")} 📌 {subToSend} *** 🗓\r\n--- события сегодня ---\r\n");
            foreach(MyEvent myEvent in _todayEvents[subToSend])
            {
                sb.Append($"\r\n🔸 {myEvent.BriefContent}\r\n");
            }
            // И клавиатуру для получения подробностей о событии
            Menu keyboard = new(Menus.Custom, isReplyKeyboard: false);
            foreach(MyEvent myEvent in _todayEvents[subToSend])
            {
                Button button = new(myEvent.Callback, $"Подробнее 🔹 {myEvent.BriefContent}");
                keyboard.AddButtonsRow(button);
            }

            // теперь можно рассылать
            foreach (MyUser user in _users.Values)
            {
                if (user.UiContext.CurrentStateEnum is not UiStates.Main)
                {
                    continue;
                }

                if (user.SubsPublished.ContainsKey(subToSend) && !user.SubsPublished[subToSend])
                {
                    _ = _ui.SendMessageAsync(user.UserData, sb.ToString(), keyboard);
                    user.SubsPublished[subToSend] = true;
                }
            }
            _lastPublishingDate = now;
            await Task.Delay(30000, CancellationToken.None); // 300 000 = 5 минут
        }
    }
}
