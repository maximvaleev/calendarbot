﻿namespace CalendarBot;

/// <summary>
/// Хранит основные данные о пользователе, полученные из Telegram
/// </summary>
internal class UserData
{
    public long UserId { get; private set; }
    public long ChatId { get; private set; }
    public string? UserName { get; private set; }
    public string FirstName { get; private set;}
    public string? LastName { get; private set; }

    public UserData(long userId, long chatId, string firstName, string? lastName = null, string? userName = null)
    {
        UserId = userId;
        ChatId = chatId;
        UserName = userName;
        FirstName = firstName;
        LastName = lastName;
    }
}